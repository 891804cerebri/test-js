// Функции: 
// 1. именнованные
// 2. безымянные
// 3. стрелочные

// Именнованные
// function имя_функции (параметры){
//     все переменные которые 
//     объявляются здесь доступны
//     только внутри функции
// }

// Безимянные: 
// function () {

// }
// в таком виде функция не может быть использована. она может быть использована в другой функции

// (function (){

// })() - недобросовестный вызов

// стрелочная функция 
// () => {} самостоятельно существовать не может. здесь можно обратиться к глобальном 

// В js идет построчное выполнение. Но функцию можно вызывать и до ее описания
// схема: 
// 1. объявляем константы
// 2. функции.console
// 3 описание функции

// function sayHello () {
//     console.log("Hello!");
// }

// sayHello();

// Параметры: сначала выносим обязательные параметры, затем второстепенные.
// Для того, чтобы задать параметр по умолчанию надо писать 

// function sayHello (name="No name", age=30) {
//     console.log("Hello!",name,age);
// }

// sayHello("Rust", 31);

// начало
// то - как надо! 

// let a=10;
// let b=7;
// let sum=0;

// function getSum (num1, num2) {
//     const sum = num1+num2;
//     return sum;
// }

// sum = getSum(a,b); Важно! sum - присваивается значения которое дал return
// console.log(sum);

// -- конец -- 


// let a=10;
// let b=7;
// let sum=0;

// function getSum (num1, num2) {
//     const sum = num1+num2;
//     return sum;
// }

// sum = getSum(a,b); 
// console.log(sum);

// let a = 5;
// let b = 10;
// let result=0;

// function operation (num1, num2) {
//     let res = num1*num2;
//     return res;
// }
//     result = operation(a,b);

// console.log(result);

// function sayHello (name="No name", age=30) {
//     console.log("Hello!",name,age);
// }

// sayHello("Rust", 31);

// let a=10;
// let b=7;
// let sum=0;

// switch(a){
//     case 10:
//         console.log("Hello!");
//         break;

//     default:
//         console.log("Bye");
//         break;
// }

//Д/з: написать функцию которая в качестве параметра будет принимать число и возвращать его текстовое название. от 1 до 5 обрабатывает, а 6 - не знаю. написал 1 - выводит один