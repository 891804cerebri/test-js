// В JS доступен такой класс как math - набор готовых математических операций: мы пишем Math. и выбираем из выпадающего списка то, что надо
// У объектов свойства, у классов - поля. 
// Деструктурированные массив - набор элементов массива, разобранный (spread).
// let b = [1,2,5,8,7];
// Для того чтобы деструктурировать массив Math.min(...b) три точки



//Округлегие
// let a = 2.6; 

// const result = Math.round(a);
// console.log(result);

//Округлегие вниз
// let a = 2.9; 
// const result = Math.floor(a);
// console.log(result);

//Округлегие вверх
// let a = 2.01; 
// const result = Math.ceil(a);
// console.log(result);

// Деструктуризация массива и поиск минимального значения массива
// let a = 2.01; 
// let b = [-1,8,6,5,4];
// const result = Math.min(...b);
// console.log(result);

// Генерация случайных чисел
// const result = Math.random();
// console.log(result);

// Генерация случайных чисел от и до
// function getRandom(from,to){
//     const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//     return random;
// }
// const r = getRandom(0,100);
// console.log(r);


// const a = 123456789123456789;
// const b = 123456789123456789;
// Чтобы сложить такое - надо сделать строквое преобразование и затем склеить

