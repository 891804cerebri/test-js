let arr = [1,4,5,6,2,4,5,1,5,6];

// for (let i=0; i<arr.length; i++) {
//     console.log(arr[i]);
// }

//     arr.forEach(showItem);

// function showItem (_, _, s) {
//     console.log(s);
// }

// Безимянная
    // arr.forEach(function (item, index) {
    //     console.log(item, index);
    // });


//Стрелочная функция промежуточный этап
// arr.forEach((item,index)=>console.log(index,item));


//Стрелочная функция
// arr.forEach(item=>console.log(item)) forEach - не возвращает значения

// Методы которые влияют на исходный массив
// изменение массива. к каждому элементу прибавить 2
// метод перебора массива map - на вход функция и на выход он будет отдавать индекс/ map не виляет на исходный массив, а создает новый

// function increment (value) {
//     return value+2;
// }
// arr.forEach(item=>console.log(item));
// const incrArr = arr.map(increment);
// incrArr.forEach(item=>console.log(item)); // новый массив

// function increment (value) {
//     if (value>5){
//         return value-10;
//     }
//     return value+2;
// }



// arr.forEach(item=>console.log(item));
// const incrArr = arr.map(value => value +10);

// const incrArr = arr.map(value => {
//     if (value>5){
//         return value-10;
//     }
//     return value+2;
// });

// incrArr.forEach(item=>console.log(item)); // новый массив

// Заполнение массива генератором случайного числа

//push - не меняет ссылку на массив. массив увеличивается на 1 элемент. 
// если надо изменить ссылку на массив randomArr = [randomArr, random]; пересоздание ссылки на массив. массив деструктуризируется и заменяется новыми значениями по новой ссылке

// function getRandom(from,to){
//         const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//         return random;
//     }

// const randomArray = [];

// for(let i=0; i<15; i++){
//     const random = getRandom(-20,50); 
//     randomArray.push(random); // добавляет элементы в конец массива
   
// }

// console.log(randomArray);

// const negativeValues = randomArray.filter(item => item <0); //метод фильтр так же как и метод map возвращает новый
// console.log(negativeValues);


// метод файнд
// function getRandom(from,to){
//     const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//     return random;
// }

// const randomArray = [];

// for(let i=0; i<15; i++){
// const random = getRandom(-20,50); 
// randomArray.push(random); // добавляет элементы в конец массива

// }

// console.log(randomArray);

// const negativeValues = randomArray.filter(item => item <0); //метод фильтр так же как и метод map возвращает новый
// console.log(negativeValues);

// const firstNegative = randomArray.find(item => item <0);
// console.log(firstNegative);


// function getRandom(from,to){
//     const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//     return random;
// }

// const randomArray = [];

// for(let i=0; i<15; i++){
// const random = getRandom(-20,50); 
// randomArray.push(random); // добавляет элементы в конец массива

// }

// console.log(randomArray);

// const negativeValues = randomArray.filter(item => item <0); //метод фильтр так же как и метод map возвращает новый
// console.log(negativeValues);

// const firstNegative = randomArray.find(item => item <0);
// console.log(firstNegative);

// function getRandom(from,to){
//     const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//     return random;
// }

// some - ищет хотя бы одно совпадение. 

// function getRandom(from,to){
//     const random = Math.floor(from - 0.5 + Math.random()*(to - from +1));
//     return random;
// }

// const randomArray = [];

// for(let i=0; i<15; i++){
// const random = getRandom(-5,5); 
// randomArray.push(random); // добавляет элементы в конец массива

// }

// console.log(randomArray);

// const negativeValues = randomArray.filter(item => item <0); //метод фильтр так же как и метод map возвращает новый
// console.log(negativeValues);

// const firstNegative = randomArray.find(item => item <0);
// console.log(firstNegative);



// const isHaveNegative = randomArray.some(item => item < -10);

// console.log(isHaveNegative);

// function accumSumm(accum, item) {
//     return accum += item;   
// }

// const summ = arr.reduce(accumSumm, 0); 

// console.log(summ);

// const summ = arr.reduce((accum, item) => accum += item, 0);   
// console.log(summ);

// Этапы преобразования обычной функции в стрелочную: 
// 1. Убираем function и название функции
// 2. между круглыми функциями с аргументами ставим =>
// 3. смотрим в тело функции - если действие одно - убираем фигурные скобки
// 4. если нет фигурных скобок убираем ретёрн 
// 5. закрываем в фигурные скобки
// 6. стрелочные функции используется в качестве аргументов других методов 

// методы управления массивами: 
// 1. arr.pop() - удаляет последний элемент массива и возвращает его
// 2. arr.push - добавляет элемент в конец массива
// 3. arr.shift - удаляет первый элемент массива и возвращает его. 
// 4. unshift(item) - добавляет элемент в начало массива
// 5. splice(индекс элемента с которым мы хотим поработать(index), количество(count), ) - от того как вы зададите сюда параметры будет меняться его поведение.
// если значения count - указать 0 то надо указывать третье свойств item - что мы хотим добавить в позицию после указанного index. Если указать в качестве count 
// иное от 0 значение - то тогда произойдет удаление указанного индекса и следующего (если указали в качестве значения 2). Если в этом случае указали item 
// то произойдет замена удаленных элементов на item 
// 6. .indexof(item) item - искомый элемент. Поиск по значению элемента, если не нашел то возвращает -1. 

//Примеры: 

// console.log(arr);
//  const lastItem = arr.pop();
// console.log(arr);
// console.log(lastItem);

// console.log(arr);
//  const lastItem = arr.push(100);
// console.log(arr);
// console.log(lastItem);

// console.log(arr);
// const lastItem = arr.unshift(100);
// console.log(arr);
// console.log(lastItem);

// console.log(arr);
//(10) [1, 4, 5, 6, 2, 4, 5, 1, 5, 6]
// arr.splice(2,1);
// console.log(arr);
// (9) [1, 4, 6, 2, 4, 5, 1, 5, 6]

// console.log(arr);
// arr.splice(2,2,...[100,100,100,100,100]);
// console.log(arr);

// console.log(arr);

// let number=prompt('Введите число которое вы хотите заменить в массива');
// let number2=prompt('Введите число которое хотите поместить в массив');



// function replaceNum (search, replace, inc_arr) {
//     const arr = [...inc_arr]; 
//     let index=arr.indexOf(search); 
//     let b=arr.splice(index, 1, replace); 
//     return b; 
// }

// const R = replaceNum(+number, +number2, arr);

// console.log(R);


// function replaceAll (search, replace, inc_arr) {
//     const arr = [...inc_arr];
//     return arr.map(item => {
//         if(item === search) {
//             return replace; 
//         }
//         return item; 
//     });
// }

// arr = replaceAll(1, 200, arr);  

// console.log(arr);

